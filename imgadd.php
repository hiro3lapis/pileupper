<pre class="crayon-selected">
<?php
 error_reporting('E_ALL');
 ini_set('display_errors', 'On');
//下の変数FILESはファイルが送信された時に自動的に格納される予約変数
 if(!empty($_FILES)){
   $file = $_FILES['image'];

   $msg = '';
   $img_path ='';//画像の表示パス

   include('upload.php');
   }

  ?>
  <!DOCTYPE html>
  <html lang="ja">
    <head>
      <meta charset="utf-8">
      <title>selfi画像投稿</title>
      <link rel="stylesheet" href="style.css">
    </head>
    <body>
      <p>
      <?php if(!empty($msg)) echo $msg; ?>
    </p><!--もし変数msgに中身があるのなら、echoで表示する -->

      <h1>画像アップロード</h1>

      <form class=""  method="post" enctype="multipart/form-data">
    <!-- enctype multipart form dataは画像などのファイルを送信する時に必要な属性-->
        <input type="file" name="image">
        <input type="submit" name="" value="アップロード">

      </form><!--imgが送信されてパスがあるのなら、php文でパスを表示しなさい -->
      <?php if(!empty($img_path)) { ?>
        <div class="img_area">
          <p>アップロードした画像</p>
          <img src="<?php  echo $img_path; ?>">
        </div>
    <?php } ?>

    </body>
  </html>
  </pre>
