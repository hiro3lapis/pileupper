<?php
  debugLogStart();
  
  if(!empty($_SESSION['login_date']) ){
    debug('ログイン済みユーザーです。');

    //現在日時が最終ログイン日時＋有効期限を超えていた場合
    if( $_SESSION['login_date'] + $_SESSION['login_limit'] < time()){
      debug('ログイン期限オーバーです。');

    //セッションを削除
    session_destroy();
    //ログインページへ
    header("Location:login.php");
  } else {
    debug('ログイン有効期限内です。');

    //最終ログイン日時を現在日時に更新
    $_SESSION['login_date'] = time();
    /* loginからmypageへアクセスする時にauthの処理が走ると
    無限ループが走ってしまう。
    basename関数を使ってmypageへ遷移するのはlogin.phpから
    遷移してきた時のみに限定することでループを回避する。
    mypage以外のファイルでauthを実行した場合nは上のlogin_dateの
    更新のみが行われる。
  　*/


    if(basename($_SERVER['PHP_SELF']) === 'login.php'){
      debug('マイページへ遷移します。');
      header("Location:mypage.php");
    }

  }

}else {
  debug('未ログインユーザーです。');
  if(basename($_SERVER['PHP_SELF']) !== 'login.php'){

    header("Location:login.php");
  }
}
 ?>
