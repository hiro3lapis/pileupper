<?php
  require('function.php');
  debug('　　　　　　　　　　　　　　　');
  debug('退会ページ');
  debug('　　　　　　　　　　　　　　　');
  debugLogStart();

  require('auth.php');
  //POST情報として、退会（submit）が入っている。
  if(!empty($_POST)){
    debug('POST送信あり');

    try{
      $dbh = dbConnect();

      $sql1 = 'UPDATE users SET delete_flg = 1 WHERE id = :us_id';
      $sql2 = 'UPDATE todo SET delete_flg = 1 WHERE id = :us_id';

      $data = array(':us_id' => $_SESSION['user_id']);

      $stmt1 = queryPost($dbh, $sql1, $data);
      $stmt2 = queryPost($dbh, $sql2, $data);

      if($stmt1){
        session_destroy();
        debug('セッション変数の中身：'. print_r($_SESSION, true));
        debug('トップページへ遷移します。');
        header('Location:index.php');
      } else {
        debug('クエリが失敗しました。');
        $err_msg['common'] = MSG07;
      }
    } catch (Exception $e){
      error_log('エラー発生；'. $e->getMessage());
      $err_msg['common'] = MSG07;
    }
   }
   debug('退会処理終了');
   ?>
   <?php
   $siteTitle = '退会';
     require('head.php');
   ?>
   <section class="site-width">
     <form class="" action="" method="post">
     <div class="withdraw">
      <input type="submit" class="btn btn-mid" value="退会する" name="submit">
     </div>
   </form>
   </section>
