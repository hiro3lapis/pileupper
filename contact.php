<?php
  error_reporting(E_ALL);
  ini_set('display_errors', 'On');

  if(!empty($_POST)){

    $err_msg = array();



    if(empty($_POST['email'])){
      $err_msg['email'] = MSG01;
    }
    if (empty($_POST['message'])){
    $err_msg['message'] = MSG01;
    }

    if(empty($err_msg)){

      $email = $_POST['email'];
      $message = $_POST['message'];

      if(!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $email)){
        $err_msg['email'] = MSG02;
      }

      if(mb_strlen($message) > 100){

      $err_msg['message'] = MSG10;
      }

      if (empty($err_msg)){

        $dsn = 'mysql:dbname=selfi_member;localhost;charset=utf8';
        $user = 'root';
        $password = 'root';
        $options = array(

          PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
          PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
          PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true,
        );

        $dbh = new PDO($dsn, $user, $password, $options);
        $stmt =$dbh ->prepare('INSERT INTO message (email, message) VALUES (:email, :message)');

        $stmt-> execute( array('email' =>$email, 'message' =>$message));

        header('Location:contact.php');
      }

    }
  }
?>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="contactstyle.css">
    <link href="https://fonts.googleapis.com/css?family=Enriqueta&display=swap" rel="stylesheet">
    <title>コンタクトフォーム</title>
    <link rel="shortcut icon" href="selfi.png" type="image/x-icon">
  </head>
  <body>

    <header>
      <nav>
        <ul>
          <li><a  href="#about" name="about">SELFiとは</a></li>
          <li><a  href="#merits" name="merits">機能・特徴</a></li>
          <li><a  href="contact.php">コンタクト</a></li>
          <li><a  href="login.html">ログイン</a></li>
          <li><a  href="account.php">新規登録</a></li>
        </ul>
      </nav>
  </header>

<!--フォーム -->
    <section class="contact">
    <h2 class="site-width">Contact</h2>
       <form   method="post">

         <span class="err_msg"><?php if(!empty($err_msg['email']))  echo $err_msg['email']; ?></span>
         <input type="text" name="email" placeholder="E-mail">

         <span class="err_msg"><?php if(!empty($err_msg['message']))  echo $err_msg['message']; ?></span>
         <textarea  id="count-text" cols="50"rows="10"type="text" name="message" placeholder="内容"></textarea>
         <div class="text-counter"><span class="show-count-text">0</span>/100

         </div>

         <div class="submit">
         <input type="submit"  value="送信">
         </div>

         <p>※入力できる文字数は１００字以内までです。</p>

       </form>
    </section>

<footer>
  ©︎CopyRightひろAllReserved
</footer>

<script src="contact.js"></script>

  </body>
</html>
