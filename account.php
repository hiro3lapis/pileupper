<?php
    require('function.php');


    //1.情報が送信されているかチェック
    if(!empty($_POST)){
      //変数にユーザー情報を代入
      $name = $_POST['name'];
      $email = $_POST['email'];
      $pass = $_POST['pass'];
      $pass_re = $_POST['pass_re'];


    //2.バリデーションチェック開始
    //入力情報が空だった場合
    validRequired($name, 'name');
    validRequired($email, 'email');
    validRequired($pass, 'pass');
    validRequired($pass_re, 'pass_re');


    //3.入力内容の具体的チェックをする
    if(empty($err_msg)){

      //nameのバリデーション
      validMaxLen($name, 'name');
      validNameDup($name, 'name');

      //Emailのバリデーション
      validEmail($email, 'email');
      validMaxLen($email, 'email');
      validEmailDup($email);


      //パスワードのバリデーション
      validHalf($pass, 'pass');
      validMaxLen($pass, 'pass');
      validMinLen($pass, 'pass');

      //パスワード再入力のバリデーション
      validMaxLen($pass_re, 'pass_re');
      validMinLen($pass_re, 'pass_re');
      validMatch($pass, $pass_re, 'pass_re');

      //エラーが空の場合、Dbへの接続準備
      if(empty($err_msg)){

        //例外処理の設定
        try {

        $dbh = dbConnect();
        //SQL文作成
        $sql = 'INSERT INTO users (name,email,password,login_time,create_date) VALUES(:name,:email,:pass,:login_time,:create_date)';
        $data = array(':name' => $name,':email' => $email, ':pass' => password_hash($pass, PASSWORD_DEFAULT),
                       ':login_time' => date('Y-m-d H:i:s'),
                       ':create_date' => date('Y-m-d H:i:s'));
        //クエリ実行
        $stmt = queryPost($dbh, $sql, $data);

      // クエリ成功の場合
      if($stmt){
        //ログイン有効期限（デフォルトを１時間とする）
        $sesLimit = 60*60;
        // 最終ログイン日時を現在日時に
        $_SESSION['login_date'] = time();
        $_SESSION['login_limit'] = $sesLimit;
        // ユーザーIDを格納
        $_SESSION['user_id'] = $dbh->lastInsertId();

        header("Location:mypage.php");
      }
    } catch (Exception $e) {
      error_log('エラー発生:' . $e->getMessage());
      $err_msg['common'] = MSG07;
     }
     }
    }
  }
 ?>
 <?php
  $siteTitle = '登録フォーム';
  require('head.php');
  ?>

  <body>

<?php
  require('header.php');
  ?>

    <section class="account-form">
      <h1>アカウント登録フォーム</h1>
      <form class=""  method="post">
        <div class="area-msg">
          <?php if(!empty($err_msg['common'])) echo $err_msg['common']; ?>
        </div>

        <span class="err_msg"><?php if(!empty($err_msg['name'])) echo $err_msg['name']; ?></span>
        <input type="text" name="name" placeholder="ユーザー名" value="<?php if(!empty($POST['name'])) echo $_POST['name']; ?>">

        <span class="err_msg"><?php if(!empty($err_msg['email'])) echo $err_msg['email']; ?></span>
        <input type="text" name="email" placeholder="Email" value="<?php if(!empty($POST['email'])) echo $_POST['email']; ?>">

        <span class="err_msg"><?php if(!empty($err_msg['pass'])) echo $err_msg['pass']; ?></span>
        <input type="text" name="pass" placeholder="パスワード" value="<?php if(!empty($POST['pass'])) echo $_POST['pass']; ?>">

        <span class="err_msg"><?php if(!empty($err_msg['pass_re'])) echo $err_msg['pass_re']; ?></span>
        <input type="text" name="pass_re" placeholder="パスワード（再入力）" value="<?php if(!empty($POST['pass_re'])) echo $_POST['pass_re']; ?>">

        <div class="submit">
          <input type="submit"  value="送信">
        </div>

      </form>
  </section>

  <footer>
    ©︎CopyRightひろAllReserved
  </footer>
  </body>
</html>
