<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <title><?php echo $siteTitle; ?></title>
    <link href="https://fonts.googleapis.com/css?family=Enriqueta&display=swap" rel="stylesheet">
    <link rel="shortcut icon" href="selfi.png" type="image/x-icon">
    <link rel="stylesheet" href="style.css">
  </head>
