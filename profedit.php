<?php
  require('function.php');
  debugLogStart();
  debug('プロフィール変更ページです。');

  require('auth.php');
//idを元に、usersテーブルから情報を取得
  $dbFormData =getUser($_SESSION['user_id']);

  if(!empty($_POST)){

    debug('POST送信があります。');
    debug('POST情報：'.print_r($_POST,true));
    debug('FILE情報；'.print_r($_FILES,true));

    $name = $_POST['name'];
    $email = $_POST['email'];
    $pass = $_POST['pass'];
    $pass_re = $_POST['pass_re'];
//ユーザー画像を表示したいが、それはまた今度

//バリデーションチェックname->email->pass->pass_reの順
  validRequired($name, 'name');
  validMaxLen($name, 'name');

  validRequired($email, 'email');
  validEmail($email, 'email');
  validEmailDup($email);
  validMaxLen($email, 'email');

  validRequired($pass, 'pass');
  validHalf($pass, 'pass');
  validMaxLen($pass, 'pass');
  validMinLen($pass, 'pass');

  validRequired($pass_re, 'pass_re');
  validMaxLen($pass_re, 'pass_re');
  validMinLen($pass_re, 'pass_re');
  validMatch($pass, $pass_re, 'pass_re');

  if(empty($err_msg)){

    try {
      $dbh = dbConnect();
      $sql = 'UPDATE users SET name =:u_name, email = :email, password = :pass WHERE id = :u_id';
      $data = array(  ':u_name' => $name, ':email' => $email, 'pass' => $pass);

      $stmt = queryPost($dbh, $sql, $data);

      if($stmt){
        debug('クエリ成功です。');
      } else {
        debug('クエリ失敗・・・。');
      }
    } catch (Exception $e){
      debug('エラー発生：'. $e->getMessage());
      $err_msg['common'] = MSG07;
    }
  }
}
  debug('profEdit終了');
 ?>
<?php
  $siteTitle = 'アカウント編集';
  require('head.php');
  ?>
  <body>
    <?php
     require('header.php');
     ?>
     <section class="edit-form">
       <form class=""  method="post">
         <div class="area-msg">
           <?php if(!empty($err_msg['common'])) echo $err_msg['common']; ?>
         </div>

         <span class="err_msg"><?php if(!empty($err_msg['name'])) echo $err_msg['name']; ?></span>
         <input type="text" name="name" placeholder="ユーザー名" value="<?php if(!empty($POST['name'])) echo $_POST['name']; ?>">

         <span class="err_msg"><?php if(!empty($err_msg['email'])) echo $err_msg['email']; ?></span>
         <input type="text" name="email" placeholder="Email" value="<?php if(!empty($POST['email'])) echo $_POST['email']; ?>">

         <span class="err_msg"><?php if(!empty($err_msg['pass'])) echo $err_msg['pass']; ?></span>
         <input type="text" name="pass" placeholder="パスワード" value="<?php if(!empty($POST['pass'])) echo $_POST['pass']; ?>">

         <span class="err_msg"><?php if(!empty($err_msg['pass_re'])) echo $err_msg['pass_re']; ?></span>
         <input type="text" name="pass_re" placeholder="パスワード（再入力）" value="<?php if(!empty($POST['pass_re'])) echo $_POST['pass_re']; ?>">

         <div class="submit">
           <input type="submit"  value="送信">
         </div>

       </form>
   </section>
     </section>
  </body>
</html>
