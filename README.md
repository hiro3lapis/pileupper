# pileupper
筋トレの記録を通じてトレーニーの交流ができるSNSアプリです。

## 機能・技術
##### PHP
##### MAMP（phpMyAdmin)
##### CDN(jquery,FontAwesome)

### 登録関連
##### ユーザー登録・退会（論理削除）
##### ログイン・ログアウト
##### プロフィール登録・編集
##### パスワードリマインド

### 筋トレレコード関連
##### レコード登録・編集
##### レコード検索（カテゴリー・日時）
##### 一覧・詳細表示
##### お気に入り機能
##### メッセージ投稿

### jquery
##### フッター固定
##### トグルメッセージ
##### 画像のライブプレビュー
##### Ajax通信（いいねボタン押下が即時反映）

###  改善点
##### サーバーに上げてのデプロイ
##### テストコード（PHPUnit)の実装
##### Ajax通信でお気に入り数を即時反映 完了（9/4)
##### Ajax通信でメッセージを送信・即時反映　->中断（送信はできたがviewに反映できず）
##### 変数名・HTML・インデント等のリファクタリング->完了(9/5)
##### レスポンシブ対応
