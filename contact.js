  window.addEventListener('DOMContentLoaded',
    function(){
      //第一引数にイベント名、第二引数に関数（実行したい処理）
      //テキストエリアの文字数を取得
      var node = document.getElementById('count-text');
      //count-textというIDのdocumentオブジェクトを取得

      node.addEventListener('keyup', function(){
      //keyupした時に関数を実行。

        //テキストの中身を取得くし、その文字数（length)を数える(数値なのでINT型）      )
        //thisはnode,count-text IDの付いたテキストエリアを意味する
        var count = this.value.length;

        //HTML５から使えるquerySelecterを使ったDOMの取得パターン
        //カウンターを表時する場所のDOMを取得する
        var counterNode = document.querySelector('.show-count-text');

        //innerTextを使うと取得したDOMの中身のテキストを書き換えられる
        counterNode.innerText = count;

      },false);

    },false
);
