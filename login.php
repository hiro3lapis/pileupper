<?php

require('function.php');

//POST送信を受けているかチェック
if(!empty($_POST)){
  //バリデーションチェック追加すること

  //変数にユーザー情報を代入
  $email = $_POST['email'];
  $pass =$_POST['pass'];
  $pass_save =(!empty($_POST['pass_save'])) ? true : false;

  //未入力チェック　
  validRequired($email, 'email');
  validRequired($pass, 'pass');

  //emailの形式チェック
  validEmail($email, 'email');
  //emailの最大文字数チェック
  validMaxLen($email, 'email');

  //パスワードの半角英数字チェック
  validHalf($pass, 'pass');

  //パスワードの最大文字数チェック
  validMaxLen($pass, 'pass');

  //パスワードの最小文字数チェック
  validMinLen($pass, 'pass');


  if(empty($err_msg)){
    debug('バリデーションOK！');

    //DBへの接続、例外処理の準備
    try {

      $dbh = dbConnect();
      //SQLで入力されたemailの値を元にpasswordとidを検索
      $sql = 'SELECT password,id FROM users WHERE email = :email AND delete_flg = 0';
      //入力したemailを変数に代入
      $data = array(':email' => $email);
      //クエリ実行$dbhはDB接続に関する設定、$sqlは照合するアカウント検索クエリ、$dataはフォームから受け取ったemailの入力値
      $stmt = queryPost($dbh, $sql, $data);
      $result = $stmt->fetch(PDO::FETCH_ASSOC);//SELECTで見つけたid,passのうち頭のpasswordを取ってくる

      debug('クエリ結果の中身：' . print_r($result, true));

      //パスワード照合
      if(!empty($result) && password_verify($pass, array_shift($result))){
        debug('パスワードがマッチ！');

        //ログイン有効期限デフォルト１h
        $sesLimit = 60 * 60;
        //最終ログイン日時を現在時刻に
        $_SESSION['login_date'] = time();

        //ログイン保持にチェックがある場合
        if($pass_save){
          debug('ログイン保持されています。');
          //ログイン有効期限を３０日に
          $_SESSION['login_limit'] = $sesLimit * 24 * 30;
        } else {
          debug('ログイン保持にチェックはありません。');
          //次回からログイン保持しないのでログイン有効期限を１hにする
          $_SESSION['login_limit'] = $sesLimit;
        }
        //ユーザーIDを格納
        $_SESSION['user_id'] = $result['id'];

          debug('セッション変数の中身：'.print_r($_SESSION,true));
          debug('マイページへ遷移します。');

          header('Location:mypage.php');

        } else {
          debug('パスワードがマッチしません。');
          $err_msg['common'] = MSG09;
        }
      } catch (Exception $e) {
        error_log('エラー発生：' . $e->getMessage());
        $err_msg['common'] = MSG07;
      }
    }
  }
 ?>
 <!DOCTYPE html>
<?php
  $siteTitle = 'ログイン';
  require('head.php');
  ?>
  <body>
<?php
  require('header.php');
  ?>

    <h1 class="top-baner">Rewrite selfimage, Chanege the life,<br>
                         with selfi.</h1>

     <section class="account-form">
       <h1>ログインフォーム</h1><!-- 下のformにaction index.htmlを入れるかどうか不明-->
       <form class=""  method="post">
         <div class="area-msg">
           <?php if(!empty($err_msg['common'])) echo $err_msg['common']; ?>
         </div>
         <span class="err_msg"><?php if(!empty($err_msg['email'])) echo $err_msg['email']; ?></span>
         <input type="text" name="email" placeholder="Email" value="<?php if(!empty($POST['email'])) echo $_POST['email']; ?>">

         <span class="err_msg"><?php if(!empty($err_msg['pass'])) echo $err_msg['pass']; ?></span>
         <input type="text" name="pass" placeholder="パスワード" value="<?php if(!empty($POST['pass'])) echo $_POST['pass']; ?>">
         <div class="btn-container">
           <input type="checkbox" name="pass_save" class="btn btn-mid">次回ログインを省略する
         </div>

         <input type="submit"  value="送信">

       </form>
     </section>




<footer>
  ©︎CopyRightひろAllReserved
</footer>


  </body>
</html>
